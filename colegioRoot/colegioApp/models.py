from django.db import models

class Student(models.Model):
    """This class Student contains information about students"""

    id_std = models.CharField(max_length=10,primary_key=True)
    name_std = models.CharField(max_length=50)
    age_std = models.IntegerField()
    email_std = models.EmailField()

    def __str__(self):
        return self.name_std



class Teacher(models.Model):
    """docstring for Teacher"""

    name_tcr = models.CharField(max_length=50)
    tel_tcr = models.CharField(max_length=10)

    def __str__(self):
        return self.name_tcr



class Subject(models.Model):
    """docstring for Subject"""

    name_sbj = models.CharField(max_length=20)
    teacher_sbj = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    student_sbj = models.ManyToManyField(Student)

    def __str__(self):
        return self.name_sbj

