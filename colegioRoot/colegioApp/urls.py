from django.conf.urls import url

from colegioApp.views import index

urlpatterns = [
    url(r'^index$', index),
]